import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';









export default class NavbarHeader extends Component {
    
    render(){
        return (
           
            <nav className="w-screen  ">
                 
                <div className="w-full flex bg-white">
                <div className="w-full font-bold flex-1 text-left text-4xl text-purple-700  px-10"> <Link to="/" > To do list</Link></div> 
                <div className="w-full  flex-1 text-right pr-10 "><Link to="/create" ><FontAwesomeIcon className="h-full text-green-500 "  icon="plus-square" /></Link></div> 
                 </div> 

           
                 
            </nav>
            


        );
    }
}