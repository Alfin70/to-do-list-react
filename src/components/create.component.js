import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class CreateList extends Component {
    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeSlug = this.onChangeSlug.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state ={
            title: '',
            slug: '',
            date: new Date(),
        }
    }

    componentDidMount() {
     
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeSlug(e) {
        this.setState({
            slug: e.target.value
        });
    }


   

    onChangeDate(date) {
        this.setState({
            date: date
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const list = {
            title: this.state.title,
            slug: this.state.slug,
            date: this.state.date
        }
        console.log(list);

        axios.post('http://localhost:8000/lists/add', list)
        .then(res => console.log(res.data));

        window.location = '/'; 
    }

    

    render(){
        return (
            <div className="w-full">
       <div className="w-full bg-blue-600 text-white"> <h3>Create New List</h3></div>
       <div className="w-full px-64">
        <form onSubmit={this.onSubmit}>
        < div className="w-full text-left">
              <label className=" text-gray-600 w-full ">Title : </label>
              <input type="text" 
              required
              className="w-9/12 bg-gray-200 hover:bg-white hover:border-gray-300 focus:outline-none focus:bg-white focus:shadow-outline focus:border-gray-300 my-6 rounded-lg"
              value={this.state.title}
              onChange={this.onChangeTitle}
              />
          </div>
          < div className="w-full text-left">
              <label className="text-gray-600 w-full ">Slug : </label>
              <input type="text" 
              required
              className="w-9/12 bg-gray-200 hover:bg-white hover:border-gray-300 focus:outline-none focus:bg-white focus:shadow-outline focus:border-gray-300 my-6 rounded-lg"
              value={this.state.slug}
              onChange={this.onChangeSlug}
              />
              
          </div>

         

          < div className="w-1/3 flex my-2">
              <label className="text-gray-600 w-full text-left  ">Date:</label>
              <div className="w-9/12 border-2 rounded-lg shadow-2xl">
                  <DatePicker 
                  selected={this.state.date}
                  onChange={this.onChangeDate}
                  
                  />
              </div>
          </div>

            <div className="w-full text-right ">
                <input type="submit" value="Submit" className=" left-align bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" />
            </div>

        </form>
        </div>
      </div>
        )
    }
}