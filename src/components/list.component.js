import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


const List =  props => (
    <tr className="text-center border-b-2"> 
        <td>{props.list.status}</td>
        <td >{props.list.title}</td>
        <td>{props.list.slug}</td>
        <td>{props.list.date.substring(0,10)}</td>
        
        <td>
        <Link to={"/edit/"+props.list._id}><FontAwesomeIcon className="text-yellow-500" icon="pencil-alt" /></Link> | <a href="#" onClick={() => { props.deleteList(props.list._id) }}><FontAwesomeIcon className="text-red-500"  icon="trash-alt" /></a>
      </td>
    </tr>
)





export default class ListComponent extends Component {
    constructor(props){
        super(props);

        this.deleteList = this.deleteList.bind(this);
        this.state = {lists: []}
    }

    componentDidMount(){
        axios.get('http://localhost:8000/lists')
        .then(response=>{
            this.setState({lists: response.data})
        })
        .catch((error)=>{
            console.log(error)
        })
    }

    deleteList(id) {
        axios.delete('http://localhost:8000/lists/'+id)
          .then(response => { console.log(response.data)});
    
        this.setState({
          lists: this.state.lists.filter(el => el._id !== id)
        })
      }

      Listes() {
        return this.state.lists.map(currentlist => {
          return <List list={currentlist} deleteList={this.deleteList} key={currentlist._id}/>;
        })
      }
    
    render(){
        return (
           
            <div className="w-full px-10">
            <div className="w-full bg-blue-600 text-center text-white font-bold"><h1>Log To Do List</h1></div>
            <table className="table-fixed">
              <thead className="w-full border-2 shadow-md bg-gray-400   ">
                <tr>
                  <th className="w-1/5 px-4 py-2">Status</th>
                  <th className="w-1/5 px-4 py-2">Title</th>
                  <th className="w-2/5 px-4 py-2">Slug</th>
                  <th className="w-1/4 px-4 py-2">Date</th>
                  <th className="w-1/2 px-4 py-2">Actions</th>
                </tr>
              </thead>
              <tbody>
                { this.Listes() }
              </tbody>
            </table>
          </div>
            


        );
    }
}