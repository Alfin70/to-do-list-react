import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faPlusSquare, faTrashAlt, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import './App.css';


import NavbarHeader from './components/navbar.component' ;
import ListComponent from './components/list.component';
import CreateList from './components/create.component';
import EditList from './components/edit.component';


library.add(fab, faPlusSquare, faTrashAlt, faPencilAlt)
function App() {
  return (
    
    <Router>
    <NavbarHeader />
      <div className="App">
      <Route path="/" exact component={ ListComponent } />
      <Route path="/create" component={ CreateList } />
      <Route path="/edit/:id" component={ EditList } />

      </div>
     
    

    </Router>

  );
}

export default App;
