const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const listSchema = new Schema({
    title: {type: String, required: true},
    slug: {type: String, required: true},
    date: {type: Date, require: true},
    status: {type: Boolean, require: true}
}, {
    timestamps: true,
});

const List = mongoose.model('List', listSchema);

module.exports = List;