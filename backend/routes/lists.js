const router = require('express').Router();
let List = require('../models/list.model');

router.route('/').get((req, res) => {
    List.find()
    .then(lists => res.json(lists))
    .catch(err => res.status(400).json('Error' + err));
});

router.route('/add').post((req, res) => {
    const title = req.body.title;
    const slug = req.body.slug;
    const date = Date.parse(req.body.date);
    const status = Boolean(req.body.status);

    const newList = new List({
        title,
        slug,
        date,
        status,
    });

    newList.save()
    .then(()=> res.json('List addded')) 
    .catch(err => res.status(400).json('Error' + err));
});


router.route('/:id').get((req, res) => {
    List.findById(req.params.id)
    .then(list => res.json(list))
    .catch(err => res.status(400).json('Error' + err));
});

router.route('/:id').delete((req, res) =>{
    List.findByIdAndDelete(req.params.id)
    .then(() => res.json('Exercise deleted'))
    .catch(err => res.status(400).json('Error' + err));
});

router.route('/update/:id').post((req, res)=>{
    List.findById(req.params.id)
    .then(list => {
    list.title = req.body.title;
    list.slug = req.body.slug;
    list.date = Date.parse(req.body.date);
    list.status = Boolean(req.body.status);

    list.save()
    .then(() => res.json('Exercise Updated'))
    .catch(err => res.status(400).json('Error' + err));

    })
    .catch(err =>res.status(400).json('Error:' + err));
});



module.exports = router;
